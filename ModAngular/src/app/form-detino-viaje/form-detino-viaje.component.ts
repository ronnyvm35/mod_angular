import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, ValidatorFn} from '@angular/forms';
import { destinoViaje } from '../model/destinoViaje.model';
import { DestinoViaje_API } from '../model/DestinoViaje_API.model';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { ajax } from 'rxjs/ajax';
import { fromEvent } from 'rxjs';

@Component({
  selector: 'app-form-detino-viaje',
  templateUrl: './form-detino-viaje.component.html',
  styleUrls: ['./form-detino-viaje.component.css']
})
export class FormDetinoViajeComponent implements OnInit {

  @Output() onItemAdded: EventEmitter<destinoViaje>;
  fg: FormGroup;
  minLongitud = 5;
  searchResults: string[];

  constructor(fb: FormBuilder) { 

    this.onItemAdded = new EventEmitter();

     /* validaciones */
    this.fg = fb.group({
      nombre: ['', Validators.compose([
              Validators.required,
              this.nombreValidator,
              this.nombreValidatorParametrizable(this.minLongitud)
            ])],
      url: ['']
    });

    this.fg.valueChanges.subscribe(
      (form: any) => {
        console.log('form cambió:', form);
      }
    );
  
    this.fg.controls['nombre'].valueChanges.subscribe(
      (value: string) => {
        console.log('nombre cambió:', value);
      }
    );

  }

  ngOnInit(): void {
    //filtro de busqueda txt
    const elemNombre = <HTMLInputElement>document.getElementById('nombre');
    fromEvent(elemNombre, 'input')
      .pipe(
        map((e: KeyboardEvent) => (e.target as HTMLInputElement).value),
        filter(text => text.length > 3),
        debounceTime(120),
        distinctUntilChanged(),
        switchMap((text: string) => ajax('/assets/datos.json'))
      ).subscribe(ajaxResponse => this.searchResults = ajaxResponse.response);
  }

  guardar(nombre: string, url: string): boolean {
    let estado = false;
    const d = new destinoViaje(nombre, url, estado);
    this.onItemAdded.emit(d);
    return false;
  }

  nombreValidator(control: FormControl): { [s: string]: boolean } {
    const l = control.value.toString().trim().length;
    if (l > 0 && l < 5) {
      return {invalidNombre: true};
    }
      return null;
    }
  
    nombreValidatorParametrizable(minLong: number): ValidatorFn {
        return (control: FormControl): { [key: string]: boolean } | null => {
        const l = control.value.toString().trim().length;
          if (l > 0 && l < minLong) {
                return { 'minLongNombre': true };
            }
            return null;
        };
    }

}
