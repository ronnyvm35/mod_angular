import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../app.module';
import { destinoViaje } from '../model/destinoViaje.model';
import { NuevoDestinoAction } from '../model/destinoViajeState.model';
import { DestinoViaje_API } from '../model/DestinoViaje_API.model';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css'],
  providers: [ DestinoViaje_API ]
})
export class ListaDestinosComponent implements OnInit {
  //destinos: string[];
  //destinos: destinoViaje[];
  @Output() onItemAdded: EventEmitter<destinoViaje>;  //variable de salida para pasar argumentos
  updates:string[];

  constructor( public destinosApiClient:DestinoViaje_API,
    private store: Store<AppState>) {
    //this.destinos = ['Panama', 'Nicaragua', 'Costa Rica'];
    // this.destinos = [];
    this.onItemAdded = new EventEmitter();
    this.updates = [];

  }

  ngOnInit(): void {
    this.store.select(state => state.destinos.favorito)
    .subscribe(data => {
      const f = data;
      if (f != null) {
        this.updates.push('Se eligió: ' + f.nombre);
      }
    });
  }

  //guardar(nombre: string, url: string): boolean {
    agregado(d: destinoViaje) {    //pasando datos al evento como clase destino
    //console.log(nombre);
    //console.log(url);
    //this.destinos.push(new destinoViaje(d.nombre, d.imagenUrl));
    //console.log(new destinoViaje(nombre, url));
     
    /* disparar store event */
    // this.store.dispatch(new NuevoDestinoAction(d));

    this.destinosApiClient.add(d);
    this.onItemAdded.emit(d);
  
  }

  elegido(d: destinoViaje) {
       /* disparar store event*/
    this.destinosApiClient.elegir(d);
 
  }

}
