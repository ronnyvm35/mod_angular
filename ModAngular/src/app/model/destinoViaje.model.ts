import {v4 as uuid} from 'uuid';

export class destinoViaje {
    //nombre: string;
    //imagenUrl: string;
    selectedVar: boolean = false;   //esta linea la usaremos como bandera de seleccion elemento
    //lo que esta comentariado es lo mismo aplicado desde seteo directo al

    servicios: string[]; // propiedad de servicios para el negocio
    id = uuid();
    public votes = 0;
    //constructor
    constructor(public nombre: string, public imagenUrl: string,  selected: boolean) {
        // this.nombre = n;
        // this.imagenUrl = u;
        //this.selected = false;
        this.servicios = ['desayuno', 'cena'];
    }

    isSelected(): boolean {          //metodo de identificacion si esta seleccionado
        return this.selectedVar;
    }

    setSelected (s : boolean) { 
                //inicializar o marcar el seleccionado
        this.selectedVar = s;
    }
    voteUp(): any {
        this.votes++;
      }
      voteDown(): any {
        this.votes--;
      }
}
