import { destinoViaje} from './destinoViaje.model';
import { BehaviorSubject, Subject } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from '../app.module';
import { ElegidoFavoritoAction, NuevoDestinoAction } from './destinoViajeState.model';
import { Injectable } from '@angular/core';

@Injectable()
export class DestinoViaje_API {
  destino: destinoViaje[] = [];
  current: Subject<destinoViaje> = new BehaviorSubject<destinoViaje>(null); //objeto observable

  constructor(
    private store: Store<AppState>) {
    /* this.store
      .select(state => state.destinos)
      .subscribe((data) => {
        console.log('destinos sub store');
        console.log(data);
        this.destino = data.items;
      });
    this.store
      .subscribe((data) => {
        console.log('all store');
        console.log(data);
      }); */
  }
  add(d:destinoViaje){
    this.store.dispatch(new NuevoDestinoAction(d));
    //this.destino.push(d);
  }
  getAll(): destinoViaje[]{
    return this.destino;
  }
  getById(id:string): destinoViaje{
    return this.destino.filter(
      function(d) { 
        return d.id.toString() == id;
      }
    )[0];
  }
  elegir(d: destinoViaje) {
    //usando lambda 
    this.store.dispatch(new ElegidoFavoritoAction(d));
    /* this.destino.forEach(x => x.setSelected(false));
    d.setSelected(true);
    this.current.next(d); */ 
    /* this.destino.forEach(function (x) {
      x.setSelected(false);
      d.setSelected(true);
    })  */
  }
}