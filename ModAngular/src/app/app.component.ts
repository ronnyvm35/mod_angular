import { Component } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ModAngular';
  ///observables correspondiente a sincronizacion evaluando como triguers
  time = new Observable(
    observe => {
      setInterval( () => observe.next(new Date().toString()), 1000); 
    }
  );

  destinoAgregado(d) {
  	//alert(d.nombre);
  }
}
