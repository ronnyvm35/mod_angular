import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, OnInit, Input, HostBinding, EventEmitter, Output } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../app.module';
import { destinoViaje } from '../model/destinoViaje.model';
import { VoteDownAction, VoteUpAction } from '../model/destinoViajeState.model';

@Component({
  selector: 'app-destinos',
  templateUrl: './destinos.component.html',
  styleUrls: ['./destinos.component.css'],
  animations: [
    trigger('esFavorito', [
      state('estadoFavorito', style({
        backgroundColor: 'PaleTurquoise'
      })),
      state('estadoNoFavorito', style({
        backgroundColor: 'WhiteSmoke'
      })),
      transition('estadoNoFavorito => estadoFavorito', [
        animate('3s')
      ]),
      transition('estadoFavorito => estadoNoFavorito', [
        animate('1s')
      ]),
    ])
  ]
})
export class DestinosComponent implements OnInit {
 // @Input() nombre: string;
  @Input() destino: destinoViaje;
  @Input() posicion: number; //cariable de posicion de destinos
  // @Input('idx') posicion: number; renombrando variable
  //@HostBinding('attr.class') cssClass = "col-md-4";

  //propiedad que permite capturar el evento emitidio
  //con directica de exposicion de salida
  @Output() clicked : EventEmitter<destinoViaje>;

  constructor(private store: Store<AppState>) {
    //this.nombre = "nombre iniciado";
    this.clicked = new EventEmitter(); //cargar objeto a la propiedad
  }

  ngOnInit(): void {
  }

  // evento para evaluar y seleccionar elemento
  ir() {
    this.clicked.emit(this.destino);
    return false;
  }
  // funciones para votasion
  voteUp() {
    this.store.dispatch(new VoteUpAction(this.destino));
    return false;
  }

  voteDown() {
    this.store.dispatch(new VoteDownAction(this.destino));
    return false;
  }


}
